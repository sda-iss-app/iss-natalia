package com.sda.natalia.iss;

import com.sda.natalia.iss.console.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        LOGGER.info("Starting ISS APP...");
        Main.run();
        //trace,debug,info, warn, error - urovne logovania
    }
}
