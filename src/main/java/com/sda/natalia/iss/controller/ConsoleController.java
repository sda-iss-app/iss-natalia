package com.sda.natalia.iss.controller;

import com.sda.natalia.iss.dto.Person;
import com.sda.natalia.iss.dto.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.sda.natalia.iss.service.IssService;


import java.util.List;

public class ConsoleController {

    private IssService issService;

    public ConsoleController(IssService issService) {
        this.issService = issService;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsoleController.class);

    public Position getCurrentISSPosition(){
        LOGGER.info("\nShowing current ISS postion...\n");
        return issService.getIssPosition();
    }
    public Float getISSSpeed() {
        LOGGER.info("\nGet current ISS speed...\n");
        return 0f;
    }
    public List<Person> getListOfPeople() {
        LOGGER.info("\nShowing Astronauts...\n");
        issService.getAstronauts();
        return List.of();

    }
}
