package com.sda.natalia.iss.service;

import com.sda.natalia.iss.client.IssClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sda.natalia.iss.dto.People;
import com.sda.natalia.iss.dto.Person;
import com.sda.natalia.iss.dto.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.List;


public class IssService {

    private static final Logger LOGGER = LoggerFactory.getLogger(IssService.class);

    private static final String ASTROS_URI = "http://api.open-notify.org/astros.json";
    private static final String ISS_POSITION_URI = "http://api.open-notify.org/iss-now.json";

    private final IssClient issClient;
    private final ObjectMapper objectMapper;

    public IssService(IssClient issClient, ObjectMapper objectMapper) {
        this.issClient = issClient;
        this.objectMapper = objectMapper;
    }

    public List<Person> getAstronauts() {
        String json = issClient.get(ASTROS_URI);
        return deserializeAstronauts(json);
    }

    private List<Person> deserializeAstronauts (String json){
        try{
        var people = objectMapper.readValue(json, new TypeReference<People>() {});
        return people.getPeople();

        } catch (JsonProcessingException ex){
            LOGGER.error("Failed to deserialize", ex);
            return List.of();
        }
    }

    public Position getIssPosition() {
        String json = issClient.get(ISS_POSITION_URI);
        return deserializePosition(json);
    }

    private Position deserializePosition (String json){
        try{
            Position position = objectMapper.readValue(json, new TypeReference<Position>() {});
            return position;
        } catch (JsonProcessingException ex){
            LOGGER.error("Failed to deserialize", ex);
            return null;
        }
    }
}
