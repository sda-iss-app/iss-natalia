package com.sda.natalia.iss.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

public class Person {

    private String name;
    private String craft;
}
